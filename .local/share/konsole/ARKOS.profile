[Appearance]
ColorScheme=ARKOS
Font=Noto Sans Mono,16,-1,5,50,0,0,0,0,0

[General]
Command=/bin/bash -c tmux
Name=ARKOS
Parent=FALLBACK/

[Interaction Options]
MiddleClickPasteMode=1
TrimLeadingSpacesInSelectedText=true
TrimTrailingSpacesInSelectedText=true

[Scrolling]
HistoryMode=2
ScrollBarPosition=2
