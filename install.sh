#!/bin/bash 

# rpm-ostree upgrade
# rpm-ostree install vim tmux git
# flatpak update

cp -r ./.config ~/ 
cp -r ./.local ~/ 
cp ./.bashrc ~/ 
cp ./.vimrc ~/ 
cp ./.tmux.conf ~/ 
cp -r ./.bashrc.d ~/	
cp -r ./.icons ~/
