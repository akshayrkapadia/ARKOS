# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]
then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH



# Default bash prompt
PS1="\\n\[\033[31m\]┌─(\[\033[33m\]\\u@\\h\[\033[31m\])─[\[\033[34m\]\\w\[\033[31m\]]\\n└──╼ \[\033[33m\]\\$ \[\033[0m\]"

# Change prompt if VM
if [ "$(systemd-detect-virt 2>/dev/null)" != "none" ]; then
	PS1="\\n\[\033[34m\]┌─(\[\033[36m\]\\u@\\h\[\033[34m\])─[\[\033[31m\]\\w\[\033[34m\]]\\n└──╼ \[\033[36m\]\\$ \[\033[0m\]"
fi

export PS1

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
if [ -d ~/.bashrc.d ]; then
	for rc in ~/.bashrc.d/*; do
		if [ -f "$rc" ]; then
			. "$rc"
		fi
	done
fi

# Start tmux on launch
#if command -v tmux &> /dev/null && [ -n "$PS1"  ] && [[ ! "$TERM" =~ screen  ]] && [[ ! "$TERM" =~ tmux  ]] && [ -z "$TMUX"  ]; then
#	exec tmux
#fi

unset rc
